//
//  SearchBDController.swift
//  Business directory
//
//  Created by Amol Aher on 12/07/18.
//  Copyright © 2018 SAMADHAN KOLHE  . All rights reserved.
//

import UIKit
import CoreLocation

class SearchBDController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var txtSearchBox: UITextField!
    @IBOutlet weak var serchBGview: UIView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var directoryCollection: UICollectionView!
    
    let inset: CGFloat = 5
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 0
    let cellsPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
    var businessDirectoryArray: JSON = JSON("")
    let placeHolderText = "“Plumber, atlanta, ga” or “jamaican, restaurant, 30032”"
    var searchTest = ""
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        menuButton.setImage(#imageLiteral(resourceName: "icon-menu"), for: UIControlState.normal)
        menuButton.addTarget(self, action: #selector(self.openLeftSideMenuPage), for: UIControlEvents.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        let menu = UIBarButtonItem(customView: menuButton)
        self.navigationItem.leftBarButtonItem = menu
        self.navigationItem.addNavBarImage()
        
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = self.storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.default.menuAnimationFadeStrength = 0.7
        SideMenuManager.default.menuBlurEffectStyle = .extraLight
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = 250
        SideMenuManager.default.menuShadowOpacity = 0.5
        
    }
    
    @objc func openLeftSideMenuPage() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let copyRightLabel = UILabel()
        copyRightLabel.text = "©️ Coming Soon Nationwide 2018. All Rights Reserved."
        
        copyRightLabel.backgroundColor = ColorConstant.footerColor
        copyRightLabel.textAlignment = .center
        copyRightLabel.font = UIFont.systemFont(ofSize: 14)
        copyRightLabel.frame = CGRect(x: 0, y: self.view.frame.size.height - 20, width: self.view.frame.size.width, height: 20)
        copyRightLabel.font = UIFont.systemFont(ofSize: 12)
        self.view.addSubview(copyRightLabel)
        
        serchBGview.dropShadow(scale: true)
        
        locationManager.requestWhenInUseAuthorization()
        getAllDirectoryData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            //check  location permissions
            self.checkLocationPermission()
        }
    }
    
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                openSettingApp(message:NSLocalizedString("please.enable.location.services.to.continue.using.the.app", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                currentLocation = locationManager.location
            }
        } else {
            print("Location services are not enabled")
            openSettingApp(message:NSLocalizedString("please.enable.location.services.to.continue.using.the.app", comment: ""))
        }
    }
    
    func openSettingApp(message: String) {
        let alertController = UIAlertController (title: AppDetails.appName, message:"Enable your location service to get Nearby Business" , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        self.navigationController?.present(alertController, animated: true, completion: nil)
        // self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func SearchButtonClicked(_ sender: UIButton) {
        txtSearchBox.resignFirstResponder()
        if searchTest == "" {
            getAllDirectoryData()
        } else {
            getSearchDirectoryData(searchText: searchTest)
        }
    }
    
    //MARK:- API Integration
    func getAllDirectoryData() {
        if Connectivity.isConnectedToInternet {
            startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate)
            Alamofire.request(APIConstants.getBusinessData, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        print(jsonValue)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            self.businessDirectoryArray = jsonValue["posts"]["details"]
                        }
                        self.directoryCollection.reloadData()
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    
    func getSearchDirectoryData(searchText: String) {
        if Connectivity.isConnectedToInternet {
            startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate)//20.0085486/73.745542"
            let escapedString = (APIConstants.getSearchBusinessData+searchText+"/\(currentLocation.coordinate.latitude)"+"/\(currentLocation.coordinate.longitude)").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            Alamofire.request(escapedString!
                , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        print(jsonValue)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            self.businessDirectoryArray = jsonValue["posts"]["details"]
                        } else {
                            self.businessDirectoryArray = []
                            ReusableManager.sharedInstance.showSnackBar(alertString: "No directory found.")
                        }
                        self.directoryCollection.reloadData()
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SearchBDController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        placeholderLabel.text = ""
        let userEnteredString = textField.text
        
        searchTest = ((userEnteredString! as NSString).replacingCharacters(in: range, with: string) as NSString) as String
        if searchTest == "" {
            placeholderLabel.text = placeHolderText
        }
        return true
    }
}

extension SearchBDController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.businessDirectoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "idSearchCell", for: indexPath) as! SearchCell
        cell.dictoryNameLabel.text = self.businessDirectoryArray[indexPath.row]["BD_name"].stringValue
        cell.directoryDetailsLabel.text = self.businessDirectoryArray[indexPath.row]["owner_name"].stringValue
        let imagePath = URL(string: self.businessDirectoryArray[indexPath.row]["bd_image"].stringValue.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        cell.directoryImageView.af_setImage(withURL: imagePath!, placeholderImage: #imageLiteral(resourceName: "image-uploading")) { (response) in
            if let image = response.result.value {
                cell.directoryImageView.image = image
            } else {
                cell.directoryImageView.image = #imageLiteral(resourceName: "image-not-available")
            }
        }
        
        cell.bgView.dropShadow(scale: true)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let postController = self.storyboard?.instantiateViewController(withIdentifier: "idDerectoryDetailsController") as! DerectoryDetailsController
        postController.directoryID = self.businessDirectoryArray[indexPath.row]["BD_id"].stringValue
        self.navigationController?.pushViewController(postController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var marginsAndInsets: CGFloat = 0.0
        marginsAndInsets = inset * 2 + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
}

