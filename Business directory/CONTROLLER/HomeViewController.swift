//
//  HomeViewController.swift
//  Business directory
//
//  Created by Amol Aher on 09/07/18.
//  Copyright © 2018 SAMADHAN KOLHE  . All rights reserved.
//

import UIKit
import CoreLocation


var bannerDataArray: JSON = JSON("")
var updatedIndex = 0

class HomeViewController: UITableViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var CollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var directoryCollection: UICollectionView!
    @IBOutlet weak var serchBGview: UIView!
    @IBOutlet weak var searchSuggetionLabel: UILabel!
    @IBOutlet weak var sizeHelperLabel: UILabel!
    @IBOutlet weak var txtSearchBox: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var copyRightLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var descriptionTextview: UITextView!
    @IBOutlet weak var clearButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    let inset: CGFloat = 5
    let minimumLineSpacing: CGFloat = 0
    let minimumInteritemSpacing: CGFloat = 0
    let cellsPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
    var businessDirectoryArray: JSON = JSON("")
    var isShowSearchedRecord = false
    var searchTest = ""
    let placeHolderText = "“Plumber, atlanta, ga” or “jamaican, restaurant, 30032”"
    let suggestionLabelText = "Enter your search terms here seprating each value with a comma (,). For example, “Plumber, atlanta, ga” or “jamaican, restaurant, 30032”"
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        menuButton.setImage(#imageLiteral(resourceName: "icon-menu"), for: UIControlState.normal)
        menuButton.addTarget(self, action: #selector(self.openLeftSideMenuPage), for: UIControlEvents.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        let menu = UIBarButtonItem(customView: menuButton)
        self.navigationItem.leftBarButtonItem = menu
        self.navigationItem.addNavBarImage()
        
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = self.storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.default.menuAnimationFadeStrength = 0.7
        SideMenuManager.default.menuBlurEffectStyle = .extraLight
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = 250
        SideMenuManager.default.menuShadowOpacity = 0.5
        
        setBannerImage()
        

    }
    
    @objc func openLeftSideMenuPage() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sizeHelperLabel.text = ""
        searchSuggetionLabel.text = suggestionLabelText
       
        copyRightLabel.text = "©️ Coming Soon Nationwide 2018. All Rights Reserved."
        
        //FOR UITABLEVIEWAUTODIMENTION
        self.tableView.estimatedRowHeight = 44;
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
        copyRightLabel.frame = CGRect(x: 0, y: self.view.frame.size.height - 20, width: self.view.frame.size.width, height: 20)
        copyRightLabel.font = UIFont.systemFont(ofSize: 12)
        self.view.addSubview(copyRightLabel)
        
        serchBGview.dropShadow(scale: true)
        
        locationManager.requestWhenInUseAuthorization()
        getAllHomePageData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            //check  location permissions
            
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        updatedIndex += 1
    }
    
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                openSettingApp(message:NSLocalizedString("please.enable.location.services.to.continue.using.the.app", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                currentLocation = locationManager.location
                getSearchDirectoryData(searchText: searchTest)
            }
        } else {
            print("Location services are not enabled")
            openSettingApp(message:NSLocalizedString("please.enable.location.services.to.continue.using.the.app", comment: ""))
        }
    }
    
    func openSettingApp(message: String) {
        let alertController = UIAlertController (title: AppDetails.appName, message:"Enable your location service to get Nearby Business" , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        self.navigationController?.present(alertController, animated: true, completion: nil)
        // self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func SearchButtonClicked(_ sender: UIButton) {
        txtSearchBox.resignFirstResponder()
        if searchTest == "" {
            ReusableManager.sharedInstance.showSnackBar(alertString: "Enter your search keyword.")
        } else {
            isShowSearchedRecord = true
            getSearchDirectoryData(searchText: searchTest)
        }
    }
    
    @IBAction func ClearButtonClicked(_ sender: UIButton) {
        txtSearchBox.text = ""
        placeholderLabel.text = placeHolderText
        self.clearButtonWidthConstraint.constant = 0
        //searchSuggetionLabel.text = suggestionLabelText
        self.businessDirectoryArray = []
        //isShowSearchedRecord = false
        self.directoryCollection.reloadData()
        self.tableView.reloadData()
    }
    
    func setBannerImage() {
        if bannerDataArray.count != 0 {
            if bannerDataArray.count > updatedIndex {
                self.bannerImageView.af_setImage(withURL: URL(string: bannerDataArray[updatedIndex].stringValue)!, placeholderImage: #imageLiteral(resourceName: "image-uploading")) { (response) in
                    if let image = response.result.value {
                        self.bannerImageView.image = image
                    } else {
                        self.bannerImageView.image = #imageLiteral(resourceName: "image-not-available")
                    }
                }
            } else {
                updatedIndex = 0
                setBannerImage()
            }
        } else {
            updatedIndex = 0
            getAllHomePageBannerData()
        }
    }
    
    func getAllHomePageData() {      
        if Connectivity.isConnectedToInternet {
            startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate)
            Alamofire.request(APIConstants.getHomeData, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        print(jsonValue)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            let descriptionStr = jsonValue["posts"]["details"][0]["long_description"].stringValue
                            self.sizeHelperLabel.setHTMLFromString(htmlText: descriptionStr)
                         
                        }
                        self.tableView.reloadData()
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    
    func getAllHomePageBannerData() {
        if Connectivity.isConnectedToInternet {
           // startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate)
            Alamofire.request(APIConstants.getBannerData, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        print(jsonValue)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            bannerDataArray = jsonValue["posts"]["ads"]
                            self.setBannerImage()
                        }
                        self.tableView.reloadData()
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    
    
    func getSearchDirectoryData(searchText: String) {
        if currentLocation == nil {
            self.checkLocationPermission()
            return
        }
        if Connectivity.isConnectedToInternet {
           
            startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate) //20.0085486/73.745542"
             let escapedString = (APIConstants.getSearchBusinessData+searchText+"/\(currentLocation.coordinate.latitude)"+"/\(currentLocation.coordinate.longitude)").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            Alamofire.request(escapedString!
                , method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            self.searchSuggetionLabel.text = ""
                            self.clearButtonWidthConstraint.constant = 50
                            self.businessDirectoryArray = jsonValue["posts"]["details"]
                        } else {
                            self.businessDirectoryArray = []
                            ReusableManager.sharedInstance.showSnackBar(alertString: "No directory found.")
                        }
                        
                        self.directoryCollection.reloadData()
                        self.tableView.reloadData()
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                    self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    
    func setUIAppearance() {
        self.tableView.backgroundColor =  UIColor(red: 0.81, green: 0.92, blue: 0.84, alpha: 1.0)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        copyRightLabel.transform = CGAffineTransform(translationX: 0, y: scrollView.contentOffset.y)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if indexPath.row == 1 {
            self.directoryCollection.reloadData()
            let height = self.directoryCollection.collectionViewLayout.collectionViewContentSize.height
            self.CollectionHeightConstraint.constant = height
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return [0,2].contains(indexPath.row) && isShowSearchedRecord ? 0 : UITableViewAutomaticDimension
    }
    

}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        placeholderLabel.text = ""
        let userEnteredString = textField.text
        
        searchTest = ((userEnteredString! as NSString).replacingCharacters(in: range, with: string) as NSString) as String
        if searchTest == "" {
            placeholderLabel.text = placeHolderText
        }
        return true
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.businessDirectoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "idSearchCell", for: indexPath) as! SearchCell
        cell.dictoryNameLabel.text = self.businessDirectoryArray[indexPath.row]["BD_name"].stringValue
        cell.directoryDetailsLabel.text = self.businessDirectoryArray[indexPath.row]["owner_name"].stringValue
        let imagePath = URL(string: self.businessDirectoryArray[indexPath.row]["bd_image"].stringValue.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        cell.directoryImageView.af_setImage(withURL: imagePath!, placeholderImage: #imageLiteral(resourceName: "image-uploading")) { (response) in
            if let image = response.result.value {
                cell.directoryImageView.image = image
            } else {
                cell.directoryImageView.image = #imageLiteral(resourceName: "image-not-available")
            }
        }
        
        cell.bgView.dropShadow(scale: true)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let postController = self.storyboard?.instantiateViewController(withIdentifier: "idDerectoryDetailsController") as! DerectoryDetailsController
        postController.directoryID = self.businessDirectoryArray[indexPath.row]["BD_id"].stringValue
        self.navigationController?.pushViewController(postController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var marginsAndInsets: CGFloat = 0.0
        marginsAndInsets = inset * 2 + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
}


