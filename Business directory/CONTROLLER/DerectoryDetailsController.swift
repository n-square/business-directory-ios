//
//  DerectoryDetailsController.swift
//  Business directory
//
//  Created by Amol Aher on 12/07/18.
//  Copyright © 2018 SAMADHAN KOLHE  . All rights reserved.
//

import UIKit
import MapKit

class DerectoryDetailsController: UITableViewController, NVActivityIndicatorViewable {

    var directoryID : String!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var headerButtonView: UIView!
    @IBOutlet weak var directoryImageView: UIImageView!
    @IBOutlet weak var directoryName: UILabel!
    @IBOutlet weak var ownerNameLable: UILabel!
    @IBOutlet weak var unitNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var businessTypeLabel: UILabel!
    @IBOutlet weak var serviceOfferedLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var closeDaysLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var faxLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    let copyRightLabel = UILabel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        menuButton.setImage(#imageLiteral(resourceName: "icon-menu"), for: UIControlState.normal)
        menuButton.addTarget(self, action: #selector(self.openLeftSideMenuPage), for: UIControlEvents.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        let menu = UIBarButtonItem(customView: menuButton)
        self.navigationItem.leftBarButtonItem = menu
        self.navigationItem.addNavBarImage()
        
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = self.storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.default.menuAnimationFadeStrength = 0.7
        SideMenuManager.default.menuBlurEffectStyle = .extraLight
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = 250
        SideMenuManager.default.menuShadowOpacity = 0.5
        
    }
    
    @objc func openLeftSideMenuPage() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //FOR UITABLEVIEWAUTODIMENTION
        self.tableView.estimatedRowHeight = 25;
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        directoryName.font = UIFont.boldSystemFont(ofSize: 18)
        copyRightLabel.text = "©️ Coming Soon Nationwide 2018. All Rights Reserved."
        
        copyRightLabel.backgroundColor = ColorConstant.footerColor
        copyRightLabel.textAlignment = .center
        copyRightLabel.font = UIFont.systemFont(ofSize: 14)
        copyRightLabel.frame = CGRect(x: 0, y: self.view.frame.size.height - 20, width: self.view.frame.size.width, height: 20)
        copyRightLabel.font = UIFont.systemFont(ofSize: 12)
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
        copyRightLabel.frame = CGRect(x: 0, y: self.view.frame.size.height - 20, width: self.view.frame.size.width, height: 20)
        copyRightLabel.font = UIFont.systemFont(ofSize: 12)
        self.view.addSubview(copyRightLabel)
        
        mapView.showsUserLocation = true
        
        directoryImageView.dropShadow(scale: true)
        mapView.dropShadow(scale: true)
        headerButtonView.dropShadow(scale: true)


        getDirectoryDataByID()
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        copyRightLabel.transform = CGAffineTransform(translationX: 0, y: scrollView.contentOffset.y)
    }
    
    @IBAction func SearchButtonClickAction(_ sender: UIButton) {
        let postController = self.storyboard?.instantiateViewController(withIdentifier: "idHomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(postController, animated: true)
    }
    
    @IBAction func BackButtonClickAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Integration
    func getDirectoryDataByID() {
        if Connectivity.isConnectedToInternet {
            startAnimating(CGSize(width: 50, height: 50), message: "", messageFont: nil, type: NVActivityIndicatorType.ballClipRotate)
            Alamofire.request(APIConstants.getDirectoryDetailsData+directoryID, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200..<300).responseJSON { response in
                if (response.result.error as? AFError) != nil {
                    ReusableManager.sharedInstance.showAlert(alertString: Messages.problemConnectingServer)
                } else {
                    switch(response.result) {
                    case .success(let jsonresponse):
                        let jsonValue = JSON(jsonresponse)
                        print(jsonValue)
                        if jsonValue["posts"]["status"].stringValue == "Success" {
                            let businessDirectoryData = jsonValue["posts"]["details"]
                            self.directoryName.text = businessDirectoryData["BD_name"].stringValue
                            self.ownerNameLable.text = ": " +  businessDirectoryData["owner_name"].stringValue
                            self.emailLabel.text = ": " +  businessDirectoryData["email"].stringValue
                            self.businessTypeLabel.text = ": " +  businessDirectoryData["business_type"].stringValue
                            self.serviceOfferedLabel.text = ": " +  businessDirectoryData["services_offered"].stringValue
                            self.addressLabel.text = ": " +  businessDirectoryData["address"].stringValue + ", " +  businessDirectoryData["city"].stringValue + ", " +  businessDirectoryData["state"].stringValue + ", " +  businessDirectoryData["zip"].stringValue
                            self.closeDaysLabel.text = ": " +  businessDirectoryData["Closed_Days"].stringValue
                            self.phoneLabel.text = ": " +  businessDirectoryData["phone"].stringValue
                            self.faxLabel.text = ": " +  businessDirectoryData["fax"].stringValue
                            self.websiteLabel.text = ": " +  businessDirectoryData["website"].stringValue
                            
                            
                            let homeLocation = CLLocation(latitude: businessDirectoryData["latitude"].doubleValue, longitude: businessDirectoryData["longitude"].doubleValue)
                            self.centerMapOnLocation(location: homeLocation)
                            
                            let imagePath = URL(string: businessDirectoryData["bd_image"].stringValue.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
                            self.directoryImageView.af_setImage(withURL: imagePath!, placeholderImage: #imageLiteral(resourceName: "image-uploading")) { (response) in
                                if let image = response.result.value {
                                    self.directoryImageView.image = image
                                } else {
                                    self.directoryImageView.image = #imageLiteral(resourceName: "image-not-available")
                                }
                            }
                            self.tableView.reloadData()
                        }
                    case .failure(let error):
                        print("Err: \(error.localizedDescription)")
                    }
                }
                self.stopAnimating()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: Messages.internetMessage)
        }
    }
    
    //MAPKIT:-
    let regionRadius: CLLocationDistance = 200
    func centerMapOnLocation(location: CLLocation)
    {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        myAnnotation.title = directoryName.text
        mapView.addAnnotation(myAnnotation)
//
//        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
//                                                                  regionRadius * 2.0, regionRadius * 2.0)
//        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView()
        headerButtonView.frame.size.width = self.view.frame.size.width
        sectionView.frame = headerButtonView.frame
        sectionView.addSubview(headerButtonView)
        return sectionView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return indexPath.row == 0 ? 170 : self.ownerNameLable.text == ": " && indexPath.row == 2 ? 0 :
        self.emailLabel.text == ": " && indexPath.row == 3 ? 0 :
        self.websiteLabel.text == ": " && indexPath.row == 4 ? 0 :
        self.phoneLabel.text == ": " && indexPath.row == 5 ? 0 :
        self.faxLabel.text == ": " && indexPath.row == 6 ? 0 :
        self.businessTypeLabel.text == ": " && indexPath.row == 7 ? 0 :
        self.serviceOfferedLabel.text == ": " && indexPath.row == 8 ? 0 :
        self.addressLabel.text == ": " && indexPath.row == 9 ? 0 :
        self.closeDaysLabel.text == ": " && indexPath.row == 10 ? 0 : UITableViewAutomaticDimension
        
       // return indexPath.row == 0 ? 170 : UITableViewAutomaticDimension
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
