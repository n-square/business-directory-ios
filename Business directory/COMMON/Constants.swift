//
//  Constants.swift
//  Business directory
//
//  Created by Amol Aher on 12/07/18.
//  Copyright © 2018 SAMADHAN KOLHE  . All rights reserved.
//

import UIKit

class Constants: NSObject {
    
}

struct Messages {
    
    static let okayText = NSLocalizedString("Okay", comment: "")
    static let dismissText = NSLocalizedString("Dismiss", comment: "")
    static let problemConnectingServer = NSLocalizedString("There was a problem connecting to server. Please try again", comment: "")
    static let internetMessage = NSLocalizedString("We are unable to update content. Please check your internet connection.", comment: "")
}
struct APIConstants {

    static let base_url = "http://blackfriday52.com"
    static let endPointURL = base_url + "/Webservices/"

    static let getHomeData = endPointURL+"Get_Home_data"
    static let getBannerData = endPointURL+"Get_Ads"
    static let getBusinessData = endPointURL+"Get_All_BD"
    static let getSearchBusinessData = endPointURL+"search_bd_nearby/"
    static let getDirectoryDetailsData = endPointURL+"bd_details/"
    static let getNearByDirectoryData = endPointURL+"get_nearby_bd/"
}

struct AppDetails {
    static let appName = "Business_Directory"
    static let appKey = "b5870dde2d53046586af9beeffac154b"
    static let appTitle = "Business Directory"
}

struct ColorConstant {
    static let appColor = UIColor(red: 0.81, green: 0.92, blue: 0.84, alpha: 1.0)
    static let footerColor = UIColor(red: 0.57, green: 0.82, blue: 0.60, alpha: 1.0)
}

struct NavigationConstant {
    
}
