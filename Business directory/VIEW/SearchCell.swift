//
//  SearchCell.swift
//  Business directory
//
//  Created by Amol Aher on 12/07/18.
//  Copyright © 2018 SAMADHAN KOLHE  . All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {

    @IBOutlet weak var dictoryNameLabel: UILabel!
    @IBOutlet weak var directoryDetailsLabel: UILabel!
    @IBOutlet weak var directoryImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
}
